\chap Core View Update (GUI)

The differences in the instruction sets of MIPS and RISC-V cause significant differences in a typical core organization. Task 3 of the assignment required me to reflect them in the core view. As part of those changes, I have implemented a long-planned switch to a system that builds the scene by interpreting an \glref{SVG} file. I have recreated the visualization as a vector diagram with hooks to inject data from the simulator. To achieve a usable visualization and scene modification \glref{API}, I have extended the SVG handling library. Now, a developer can edit the scene in a \glref{GUI} editor and pay more attention to visual details.

\sec RISC-V Differences

\midinsert \clabel[rv book]{RISC-V pipeline data path from the coursebook}
\picw=\hsize \cinspic media/riscvbookc.pdf
\caption/f RISC-V pipeline data path from the coursebook. You can compare it with the original visualization of
QtMips on the next page. (Source: [\rcite[patterson2017computer], figure 4.49])
\endinsert
MIPS pipelined \glref{CPU}s can branch directly from the decode stage using equality comparison on the register values. Meanwhile, RISC-V provides more branch instructions and uses ALU to determine whether they should be taken. Because ALU is in the execute stage, the whole branching mechanism occurs in later stages. Control signals and branch destination address bus need to be delayed (saved to interstage registers). Forwarding to the decode stage, previously required by the {\tt jump and link} instruction, is no longer needed in RISC-V. To replace the decode stage comparator, a zero signal from the \glref{ALU} was added.

RISC-V ISA encodes the destination register number always to a constant location. This fact makes one multiplexer and its control signal redundant. RISC-V does not have a delay slot, and it has complex immediate decoding [\rcite[riscv], section 2.3].

\midinsert \clabel[wtmips pipeline]{Original QtMips core view at maximum complexity setting}
\picw=\hsize \cinspic media/complex_pipeline_oldc.pdf
\caption/f Original QtMips core view at maximum complexity setting.
\endinsert

\sec QtMips C++-based Visualization Framework

The core view is described entirely in the \glref{C++} code. Ing.~Karel Kočí created a framework of objects representing individual parts of the \glref{CPU} core and macros used to assemble a core visualization from them. An example of such object is a multiplexer: It has a method to paint itself, it exposes points to connect wires, and it has a slot to update its state (in this case, which input is the output connected to). Then, macros are used to add the component to the screen. The macro allocates the object, connects it to signals, and saves a pointer to the component. Later, wires could be connected.
Aside from the lack of documentation and occasional unclear naming, the framework seems well built.
However, modification of the layout is not a simple task. The first problem is to match objects with components on the screen. The second problem is that every movement requires recompiling to ensure that the changes were correct. Adding simple details to the image means creating a class, allocating an object, and positioning it manually.

\sec Svgscene Library and Its Usage

As a replacement for the direct creation of \glref{Qt} objects, QtRVSim builds the \glref{Qt} scene by parsing an
\glref{SVG} file. Dynamic values are updated using a custom \glref{API} similar to \glref{XML} Document Object Model
(DOM)\cite[w3cdom]. The library created for this purpose is based on "svgscene"\urlnote{https://github
.com/fvacek/svgscene}, "libshv"\urlnote{https://github.com/silicon-heaven/libshv} and QtSVG\urlnote{https://github.com/qt/qtsvg}.

First, \glref{SVG} is parsed, and each \glref{SVG} element is translated to a \glref{Qt} object with corresponding styles while preserving \glref{XML} attributes. Then, the elements of interest are retrieved using the document traversing \glref{API}. Then, one proceeds as with any other \glref{Qt} graphics items and objects.

In the next section, I describe how the \glref{SVG} files themselves are created. Before that, I present the document traversing \glref{API}.

\secc Document Traversing API

The key component of the \glref{API} is {\tt SvgDomTree}, which wraps the "QGraphicsItem"s in  \glref{SVG} aware wrapper. "SvgDomTree" has methods to read \glref{XML} and \glref{CSS} attributes and their values and methods for searching the scene/document tree. Methods "find" and "findAll" search child elements using a naive depth-first-search\urlnote{https://en.wikipedia.org/wiki/Depth-first_search} and return first, and all matching elements, respectively, wrapped as "SvgDomTree"s. The find methods accept the type of searched element as a template parameter, and the \glref{XML} attribute name and value to search as function parameters. All parameters are optional. The default value for the template parameter is {\tt QGraphicsItem}. The wrapped \glref{Qt} object can be obtained by a call to the "getElement" method.

\midinsert
\begtt    

    document
        .getRoot()
        .find<QGraphicsItem>("data-component", "data-cache")
        .find<SimpleTextItem>()
        .getElement();

\endtt
\sbf{Example.} {\em Finds the element corresponding to the cache components and finds a text element inside.  }
\begtt    

  for (auto hyperlink : document.getRoot().findAll<HyperlinkItem>())
  {
    this->install_hyperlink(hyperlink.getElement());
  }

\endtt
\sbf{Example.} {\em Real snippet from QtRVSim: This code searches all elements corresponding to \glref{XML} hyperlinks[\rcite[svgtinyspec], section 14] and calls a method to install them (bind them to \glref{Qt} slots). }
\endinsert

\secc Error Handling

It can often happen that no element is found. Returning a null pointer would require an enormous amount of checks which would significantly complicate the \glref{API} usage. Therefore, "SvgDomTree" can never contain a null. Any attempt to create "SvgDomTree" from null results in an exception being raised.

For compatibility with WASM, this safe behavior is only used at the top level. Internally, the "find" method, searching a single item, returns a null pointer when nothing is found. This is because WASM does not support exception handling.\urlnote{https://emscripten.org/docs/porting/exceptions.html}

\sec Core Diagram and SVG Image

Given that most parts of the image never change, it felt natural to replace them with a static vector image. \glref{SVG} fits very well this use; however, direct use of an \glref{SVG} was not ideal from a maintenance point of view. The advantage of a specialized diagramming tool over an \glref{SVG} editor is that it provides better support for working with connection lines. This includes forced orthogonal lines, automatic line crossing, connection points for custom shapes, and moving the lines together with objects.

\midinsert \clabel[new pipeline]{New SVG-base coreview}
\picw=\hsize \cinspic media/newpipeline.pdf
\caption/f New SVG-base coreview.
\endinsert

I have decided to use diagrams.net (formerly known as draw.io). It is a free, open-source, and well-documented tool working in a browser or locally. Diagrams.net has many useful features and plugins. It allowed me to keep all configurations (single cycle CPU, pipelined CPU, and pipelined CPU with forwarding) in a single diagram, reusing the common parts. I have annotated every part to belong to a subset of the listed configurations. It can easily display each of them or all at once (for layout debugging purposes). Hyperlinks are used to open related sections of the \glref{GUI} (for instance, memory view by clicking on memory). Data attributes are used to annotate components with updatable values and their data sources. A downside is that I cannot modify all objects of the same ``class'' at once. I have to replace each instance separately or copy-paste the style. Furthermore, the options to control the resulting \glref{SVG} are limited. For example, the cache statistics had to be created as a custom element to ensure that all text elements are children of the cache component.

\midinsert \clabel[draw.io]{Diagrams.net editor}
\picw=\hsize \cinspic media/diagrams.netc.pdf
\caption/f Diagrams.net editor.
\endinsert

The diagram source file is located in {\tt extras/core-graphics}. The usage of diagrams.net and the exporting process is documented in {\tt docs/developer/coreview-graphics/using-drawio-diagram.md}.

All shapes that I have used (fully custom or compositions of simpler shapes) are available on the GitHub\urlnote{https://github.com/jdupak/Diagrams.net-CPU-scheme-kit}.

\nocite[svgmdn]