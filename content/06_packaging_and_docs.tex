\chap Packaging and Documentation

This chapter is related to task four of the assignment. It starts with an analysis of the QtMips solution and the current package management situation on major operating systems. Given that we are releasing QtRVSim as a completely new project, package configuration files had to be modified and building services set up. I have simplified the release process and made it platform independent and with a single source of truth for shared information. Based on my analysis and my current knowledge, I have introduced two new packages. Finally, to build on older distribution, some parts of the code and CMake scripts needed modifications.

Regarding documentation, I tried to document everything in place of usage with code comments. For more complex operations, there are new documentation pages in the {\em docs} directory. I have also updated the {\tt README.md}; however, my information on simulation itself is limited due to the delay of my colleague working on the core.

\sec QtMips
QtMips is built, and its binary packages are distributed using openSUSE Build Service (\glref{OBS})\urlnote{https://build.opensuse.org/} for the following Linux distributions: Debian, Fedora, Raspberian, SUSE Linux Enterprise, and openSUSE. For Ubuntu Launchpad\urlnote{https://launchpad.net/} is used. \glref{OBS} also builds distribution-independent AppImage. Binaries for Windows, macOS, and \glref{WASM} are built manually and uploaded to GitHub releases.

For Debian, QtMips uses the so-called "native" format. It requires the {\em debian} directory to be in the project root and source archives. The distribution maintainers disfavor native package use for software not directly related to the particular distribution. It is preferred to store package files out of project root, and it is considered during the quality review. Solving this problem would make it easier to get the package to official repositories\fnote{An information from my supervisor.}.

\sec Linux Distributions Coverage Analysis

According to Distrowatch\cite[distrowatch], the top ten Linux distributions are based on these independent\fnote {Not derived from another distribution. Packages for the independent distributions will usually work on the derived ones as well.} distributions: Debian, Arch, Fedora, and openSUSE. From these distributions, QtMips omits only Arch Linux.

Another group of package managers, which are currently gaining popularity, is that, independent of particular distributions. They mainly aim at isolation, reproducibility, and security. The most known of those are Flatpack\urlnote{https://flatpak.org/}, Snap\urlnote{https://appimage.org/}, Nix\urlnote{https://nixos.wiki/wiki/Nix} \fnote{Nix comes with Linux distribution NixOS, but given its isolative properties, it is well usable on other systems as well. I use it on Manjaro alongside the Pacman.} and Guix \urlnote{https://guix.gnu.org/en/} \fnote{GNU package manager based on Nix}.

Package management on Windows and macOS is currently neglected. Homebrew\urlnote{https://brew.sh/} is the primary package manager on macOS. On Windows, official software distribution channels are Microsoft Store and WinGet. To support Microsoft Store, it would be necessary to compile QtRVSim as a Universal Windows Platform (\glref{UWP})\urlnote{https://docs.microsoft.com/en-us/windows/uwp/} application. That is generally not a problem as \glref{Qt} officially supports \glref{UWP}. The only problem here is to fix minor incompatibilities with the \glref{MVSC} compiler. From unofficial package managers, the most used is Chocolatey\urlnote{http://chocolatey.com/}. Chocolatey is recognized by Microsoft, and it is even the official way to install software on Windows runners of GitHub Actions\urlnote{https://docs.github.com/en/actions/using-github-hosted-runners/customizing-github-hosted-runners} which do not yet support WinGet\urlnote{https://docs.microsoft.com/en-us/windows/package-manager/winget/}.

Given my personal experience with Arch Linux and Nix, I have decided to add those packages as part of this task. In future work, I intend to add support for more of the mentioned package managers. Table \ref[github_downloads] in appendix suggests that QtMips has many Windows users. Therefore, better Windows support might be appreciated.

In the appendix, there are tables of download statistics from GitHub and Launchpad (\ref[github_downloads], \ref[launchpad_downloads]). Unfortunately, it is not possible to obtain statistics from \glref{OBS}, which would provide a broader image of the Linux situation.

\secc NIX Package
Nix package manager allows users to install software on any Linux distribution, regardless of its package and dynamic library management. This is a great advantage for the project as we can provide a package of which we can be sure that it will work on any Linux distribution, no matter how minor it is.

To achieve this, the Nix package manager installs and manages {\sbf all} dependencies (including the required version of the \glref{libc}, \glref{Qt}, etc.) in addition to those managed by the system package manager. The obvious disadvantage of wasting disk space is negligible given the current capacities of hard disks and their prices. Also, thanks to Nix design, all of these files are isolated in specialized directories and do not pollute the system.

\sec Implementation

All package-related files have been moved from the project root to {\tt extras/packaging}. They are stored in the form of CMake templates\fnote{Text files with CMake variables like this: {\tt @A\_VARIABLE@}}. In the configuration phase (the initial call to CMake), up-to-date information is injected into placeholders within the templates. After that, an {\tt open\_build\_service\_bundle} target makes a directory with all files necessary to build all supported packages. To distribute a new release, it is now sufficient to update the changelog and upload the files produced by the {\tt open\_build\_service\_bundle} target to \glref{OBS}. This procedure can be performed on any operating system with CMake, Git, and \glref{XZ}. The only limitation is that the file system where the files are produced must support file permission modification. Debian builds rely on correct file permission setting. Correct permissions are set automatically during the build whenever the file system supports it.

I have switched the Debian package to the {\em quilt}\cite[debian_man] format, which keeps the source archive intact and ships Debian-specific files in another tar.

After that, I have created Arch Linux and Nix packages. The Arch Linux package is now also built by openSUSE Build Service.

Binaries for Windows and macOS can easily be obtained from GitHub actions as they are built as part of the automatic testing \glref{CI} procedure. At this point, automatic release publishing was not a priority, but it can be achieved by extending the \glref{CI} script according to this example\urlnote{https://cristianadam.eu/20191222/using-github-actions-with-c-plus-plus-and-cmake/}.

\secc Fallbacks

To make the code compile against older versions of \glref{Qt}, I had to devise several fallbacks in the polyfill library. For versions older than 5.10 (Ubuntu 18), "QStringView" is replaced by {\tt QString}. {\tt QStringLiteral} macro is necessary for literals to work with both versions. Prior to version 5.13, \glref{Qt} objects cannot be stored in an \glref{STL} container. However, I need to store references in the \glref{GUI} controller. That is not possible in \glref{Qt} containers, where all values need to be default-constructible. To overcome this issue, I implement "std::hash" for {\tt QString} and {\tt QStringView} manually for older \glref{Qt} versions.

\secc Distributions Excluded From Support

The distributions listed below (previously supported by QtMips) are no longer supported for not providing required versions of dependencies and build tools:
\begitems
* Debian 9
* Ubuntu 16 (used minimally, zero downloads of the 2020 version, see~\ref[launchpad_downloads])
* openSUSE Leap 15.1
* default OBS AppImage (replaced by AppImage based on openSUSE Leap 15.2)
\enditems