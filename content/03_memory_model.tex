\chap Memory Model Redesign

Task 2 of the thesis assignment required me to redesign the memory system to support simulation of little- and big-\glref{endian}, 32- and 64-bit targets, and the usage of memory-mapped files\urlnote{https://www.man7.org/linux/man-pages/man2/mmap.2.html}. To satisfy the requirements, the memory hierarchy and the read/write \glref{API} had to be reworked. New types and classes have been introduced to produce cleaner and safer API. Finally, a considerable amount of refactoring and documenting was needed.

\sec Original Model

The MIPS simulator emulates only 32-bit big-\glref{endian} \glref{CPU}s. It uses word-sized\fnote{32-bit integer} functions for all memory-related operations. This way, it can transfer data through return values. The main memory is naturally represented by an array of words. In the case of a read, the memory subsystem always loads the whole word. Smaller reads are extracted from it. When data write size is not a complete 32-bit word, the whole word containing the current value has to be read first. Then the new value is packed into it, and it is written back. Given that offset accesses and memory map are not supported, the main memory can internally have the same \glref{endianness} as the host machine (the computer running the simulator). When the simulated and the host system \glref{endianness} mismatch and the access size is smaller than a word, the top-level functions compensate for the difference.

The memory subsystem consists of components, e.g., cache, memory (RAM), address space mapping component and peripherals. All components implement a "MemoryAccess" interface which defines the read/write API. Some components can form chains by accepting another instance of "MemoryAccess" as their backing memory (multi-level cache, cache and RAM). A component that receives a request either resolves it or invokes its backing component. Memory is addressed by a 32bit integer. The CPU core typically has data memory and program memory entry points.

\sec Modified Memory Hierarchy Overview

The new model recognizes two types of memory interfaces: "FrontendMemory" and "BackendMemory". Every CPU core configuration has three mandatory frontend memory components. Two of them are the entry points for memory operations for the program and the data, respectively. The entry point starts a chain of memory components of arbitrary length, each backed by the next one. For the core, the chain is invisible, and it only interfaces with the top-level component. The chain is terminated by a memory data bus. The data bus for the data memory is the third component mentioned. The frontend memory has three characteristics. Except for the memory bus, each frontend component must have a frontend component backing it. The memory bus is backed by zero or more backend devices (explained later). All addresses used in the frontend are full memory addresses. By the word {\em full}, I mean the numerical value observable by a \glref{C} program (up to address translation\urlnote{https://en.wikipedia.org/wiki/Memory_management_unit}), in contrast to a local relative offset. The {\tt Address} datatype is used to store the address and pass it as an argument. Finally, frontend memory instances are likely those found on the \glref{CPU} chip itself (cache, \glref{TLB}).

\vskip6pt


{\sbf Example.} In the simplest \glref{CPU} with no cache and joint memory address space, all three components coincide. They are all references to the same object, the data bus. It is both the entry point and the exit point of the chain.

{\sbf Example.} A more common configuration is a split single-level cache with a single memory bus (joint memory address space). The caches are the entry points here, and both of them are backed by the bus. With multi-level caches, we only have to add more cache objects to the chain. If we were to implement a \glref{TLB}, it would become the top-level component of the chain.

{\sbf Example.} When simulating a \glref{CPU} with the program and the data memory split, we create them with separate memory buses and pass the {\em data} one to the core. The core needs an access to the {\em data} memory data bus to emulate functionalities like file access using memory map at runtime. The separate program memory is read-only at the simulation runtime.

\vskip6pt

The other part of the memory model is the backend memory. Real-world examples are the main memory, memory-mapped I/O peripherals, and in our simulation also memory-mapped files from the host system. I often call backend memory instances {\em devices} as, in the real world, they are not permanent parts of the machine. Backend memory devices generally do not form chains. They can internally consist of multiple layers, but that behaviour is not a part of the backend memory interface. They connect to a memory data bus, listening for operation on a certain address range. In contrast to a typical simple hardware implementation, the address range filtering is performed by the memory bus rather than the components. In modern computers the situation is more complex (e.g. PCIe).  Each component is addressed by a local relative offset starting at zero for each component. To refer to this offset, a type alias {\tt Offset} is used for 64bit unsigned integer. The address to range conversion is a responsibility of the bus. Devices can be inserted or removed at any time, for example, by an operating system emulation.

\sec Load / Store API

The requirement to simulate both 32- and 64-bit machines makes the simple solution of fixed-width accesses unusable. Using a wider access and ignoring part or multiple narrower accesses would produce misleading statistics, mainly in caches. It would also pose a problem for the compressed instruction extension and the vector extension. Based on the suggestion of my supervisor, I have designed the \glref{API} inspired by the semantics of \glref{POSIX} functions {\tt read}, {\tt write} and {\tt memcpy}. It takes a destination, a source, a size, and, in addition to {\tt memcpy}, also an options struct and returns a result struct. The \glref{API} ensures that {\em size} bytes will be transferred from the {\em source} to the {\em destination}. In particular, a read transfers data from a simulated location specified by the source to a buffer pointed to by the destination and write transfers data from a source buffer to a simulated destination location. This way, arbitrary-sized types can be transferred, ranging from a single byte to whole cache lines. Transfers larger than 64 bits are, however, more complex due to \glref{endian} issues. For convenient use, the {\tt FrontendMemory} interface provides simplified value-passing-based entry points for basic integral types. They provide a temporary buffer and handle the \glref{endianness} internally. The complexity of the \glref{endianness} simulation is discussed later.

\midinsert
\begtt

  uint16_t value = 42;
  Write result res = some_frontend_memory.write(
      0xff_addr,
      &value,
      sizeof(value),
      WriteOptions()
  );
  // or for common integer types
  some_frontend_memory.write_u16(0xff_addr, 42);

\endtt
{\sbf Example.} {\em Demonstration of the new \glref{API} usage.}
\endinsert

The special types {\tt ReadOptions}, {\tt ReadResult}, {\tt WriteOptions} and {\tt WriteResult} keep the \glref{API} simple and extensible. Separating parameters and return values (in contrast to returning multiple values using side-effects) produces a much simpler and more predictable data flow. Both result types contain information on the successful access size. An access can happen on a boundary of some form of a region (a page, a physical \glref{RAM} module) and succeed only partially. For more advanced uses, like a vector extension, partial success is a necessary feature. Write result struct also contains the information on whether the write changes the affected memory. The options struct currently only contains a flag determining the intended effects of the read. Internal reads should not cause any side effects, and their repetition should always return the same values. Internal reads are used, for example, by the \glref{GUI} to display current memory data.

\secc Endian Simulation

There are eight possible combinations of \glref{endianness} for each memory data path. The host system, the simulated machine, and the final backend device can have any \glref{endianness}. The tricky part is maintaining the correct results for unaligned and offset accesses.

The aim is to make the host and the simulated main memory equivalent, when they are interpreted as arrays of bytes. When the stimulated and the native \glref{endianness}es are the same, the situation is trivial, and no swaps are needed. Otherwise, we need to store the value to memory the same way the simulated machine would.  When simulating little-endian on big-endian, we need to swap the value as it would be swapped when written by the simulated little-endian machine. Conversely, when simulating big-endian on little-endian, pre-swapping results in the value being stored in big-endian order as the host machine swaps the value again. (See the example below.)

\midinsert
\begtt
  
  BIG on LITTLE
      REGISTER:           12 34 56 78
      PRE-SWAP:           78 56 34 12 (still in register)
      NATIVE ENDIAN MEM:  12 34 56 78 00 00 (native is LITTLE)
      READ IN MEM:              56 78 00 00
      REGISTER:                 00 00 78 56
      POST-SWAP:                56 78 00 00 (correct)
  
  LITTLE on BIG
      REGISTER:          12 34 56 78
      PRE-SWAP:          78 56 34 12  (still in register)
      NATIVE ENDIAN MEM: 78 56 34 12 00 00 (native is BIG)
      READ IN MEM:             34 12 00 00
      REGISTER:                34 12 00 00
      POST-SWAP:               00 00 12 34 (correct)

\endtt
{\sbf Example.} {\em Correct handling of 4 bytes write followed 4 bytes read offset by 2 bytes. I assume memory to be zeroed initially. The visual offset represents the address offset. (This snippet is also a part of the inline code documentation.)}
\endinsert

All nonperipheral components (cache and main memory) store data in the same \glref{endianness}. Therefore, no swap is required between them, and data can be copied fast. That is enforceable as the data is only accessible via the memory \glref{API}. Peripherals have to behave in this manner externally, but they might be forced to use a different \glref{endianness} internally. The reason may be that their internal registers are directly observable by the UI or entirely out of simulator control. The internal storage can be a memory-mapped file from the host system, or it can even lead to a real memory-mapped hardware, which dictates the \glref{endianness}. As a proof of concept, I have created a backend device that uses an anonymous memory map to simulate the main memory instead of a malloc\urlnote{https://man7.org/linux/man-pages/man3/malloc.3.html} allocated tree [\rcite[qtmips], pg. 26]. Another option is to map an executable in this way. Each backend memory device has a public {\tt simulated\_machine\_endian} property for easier orientation. It can be constant (LCD), set according to host \glref{endianness} (main memory), or set dynamically at initialization (mapped executable).

\secc 32-bit Accessible Peripherals

When switching to the new model, modifying the peripherals to be internally byte-addressable would be too complicated. Section 2.6 of the standard\cite[riscv] allows declaring that some regions only support word access and raise an exception on a misaligned access. However, it was decided to keep the behaviour simple for a user and emulate the misaligned access within the peripherals.

\sec Refactoring

To implement the new model, I had to perform a large amount of refactoring. I mainly intended to simplify the data flow, break the code into smaller components, introduce new types and type aliases, rename too short names to more descriptive ones, and provide more comments. In the following sections, I present the most conceptually essential changes.

\secc Address Data Type
Prior to these changes, a 32-bit integer was used to store and pass a frontend memory address. This is problematic for three reasons. It is fixed to 32-bit and hard to change. It provides no documentation (see example below). Also, the compiler cannot check its usage. Now, if a function requires a simulated address, either you already have an {\tt Address}, or you have to construct it explicitly and take responsibility for its correctness.
\midinsert
\begtt

  QMap<std::uint32_t, hwBreak *> hw_breaks;
  // vs
  QMap<Address, hwBreak *> hw_breaks;

\endtt
{\sbf Example.} {\em This collection controls program locations, where \glref{CPU} core should stop execution and notify the execution environment. This feature is used mainly by debuggers.}
\endinsert

The type checking is extra helpful when using slots\fnote{Qt component communication primitive. Primarily used for asynchronous updates of the \glref{GUI}.}. The functions being connected are often from distant parts of the codebase (e.g., simulation and \glref{GUI}), which might even be maintained by different developers.
\midinsert
\begtt

  void fetch_inst_addr_value(machine::Address);
  void fetch_jump_reg_value(uint32_t);
  void fetch_jump_value(uint32_t);
  void fetch_branch_value(uint32_t);
  void decode_inst_addr_value(machine::Address);

\endtt
\sbf{Example.} {\em Some of the 58 lines declaring slots in the machine core. }
\endinsert

\secc RegisterValue Data Type

{\tt RegisterValue} type was introduced to abstract away the width of the simulated machine registers. All arithmetic operations are performed on a private 64-bit internal storage, regardless of the simulated machine's configuration.
From the type safety point of view, {\tt RegisterValue} is considered the most general integral value. All integers can implicitly {\em degrade} to it (i.e., implicit construction is allowed). In the other direction, we are assuming some interpretation of the value, and we have to do that explicitly. {\em Explicit} static casts to common integer types are implemented.
All constructors, getters, and cast operators ensure proper sign extension as required by the RISC-V standard Section 5.2:

{\em These *W instructions ignore the upper 32 bits of their inputs and always produce 32-bit signed values, i.e., bits \glref{XLEN}-1 through 31 are equal.}\cite[riscv]. (In the 64bit instruction set, "*W" stands for instructions postfixed with "W". Such instructions operate on 32bit values in 64bit registers.)

When simulating a 32-bit machine, we behave as if all instructions were of the "*W" type.

\secc Cache Replacement Policy
Cache policy replacement handling was previously tightly integrated into the cache code. I have separated it into a decoupled component with no knowledge of the cache except its dimensions. Now every policy consists of tiny blocks of code, which can be tested separately. They are also much simpler to verify by reading. These advantages are desirable as the cache simulation is likely to be extended in the future.\fnote{Even at this time, a Greek colleague is adding L2 cache simulation to the QtMips.}

\secc Extended Testing
Given the increased complexity of the memory subsystem, the current testing suite, which tested only a few configurations, seemed unsatisfactory. I have built a test generator, which tests the full cartesian product of the cache configurations. This turned out to be a good decision as it helped me discover and debug multiple serious problems. The execution time of all machine unit tests is still under one second, which is entirely negligible compared to the compilation time.
To test the correct offset behaviour, I have devised a mechanism that decomposes a 64-bit integer to all subcomponents (32, 16, 8) as if they were read on each simulated \glref{endianness} regardless of the native \glref{endianness}.
Automation of cache performance testing is problematic. Therefore, I have decided to only test for future changes. I have saved the current results to an array, and I compare them based on the order. Any change of the configuration matrix invalidates these values, and they have to be regenerated.

\secc Qt5 Signal-Slot Syntax

This part is closely related to the introduction of the address data type. The original \glref{Qt4} string-based system has proven very error-prone, and it made it hard to track all code paths, where {\tt Address} should replace {\tt uint32\_t}. With \glref{Qt4} syntax, type checks are performed when the {\tt connect} function is called, and type errors only result in runtime warnings. \glref{Qt5} syntax\urlnote{https://wiki.qt.io/New_Signal_Slot_Syntax} replaces the string names with \glref{C++} method referencing. Usage of the language native features instead of meta-object compiler immediately enables standard \glref{C++} compiler type checking.
I have upgraded all connections to \glref{Qt5} syntax to benefit from the extra type checking. Fortunately, QtCreator\fnote{An official \glref{Qt} integrated development environment.} has a semi-automatic way of converting those syntaxes, and it worked most of the time.

\midinsert
\begtt    

  connect(
      sender, SIGNAL( valueChanged( QString, QString ) ),
      receiver, SLOT( updateValue( QString ) )
  );

\endtt
\sbf{Example.} {\em Old syntax. }
\begtt

  connect(
      sender, &Sender::valueChanged,
      receiver, &Receiver::updateValue
  );

\endtt
\sbf{Example.} {\em New syntax. }
\endinsert


A minor disadvantage of the new syntax is that it does not support implicit overloading. It has to be done explicitly in quite a wordy way.

\midinsert
\begtt

  connect(ser_port, &machine::SerialPort::tx_byte,
          this, QOverload<unsigned int>::of(&TerminalDock::tx_byte));

\endtt
\sbf{Example.} {\em Problematic overloading. }
\endinsert

\sec Backporting

All the changes described here are compatible with both, QtRVSim and QtMips. The git branch has been kept separate from all RISC-V-related changes. With minor modifications to the core that Ing. Píša offered to implement on his own, the new memory model can be backported to the \glref{CTU}/QtMips repository. We intend to keep it in the main development branch for anyone who would contribute to the QtMips.

After a discussion among QtMips/QtRVSim developers, it was decided that it did not make sense to release the changes to students in the master branch. The gain for current teaching is minimal, and there is a risk of new bug introduction. We intend to concentrate our efforts on the QtRVSim. 