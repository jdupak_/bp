\chap Project Management

The title of the thesis makes me responsible for project management. This chapter discusses various subtasks not fitting under task four of the assignment. Task four (packages and documentation) is covered by the next chapter.

This part mainly relates to the directory structure and the process of building. The chapter starts with project structure changes, aiming to keep unrelated files in separate directories, especially in the project's root directory. Source code has been separated from support files, and a new subproject for shared code with a new internal compiler-compatibility library has been introduced. The project has been upgraded from deprecated QMake to CMake. Finally, a \glref{CI} multi-platform testing procedure was set up.

\sec Project Structure and Common Libraries

I have moved all source subprojects to the "src" directory, 3rd party code, and git submodules to "external" directory, and support content to {\tt extras}. In {\tt src}, I have introduced a new subproject, {\tt common}, for small custom libraries shared between the subprojects. Previously, such code was placed in the machine (previously qtmips\_machine) subproject, which was already conveniently included in other subprojects. However, I found this to be problematic. The concerned code had no direct bindings to the machine, and it should have been tested separately.

One of the libraries I have created in "common" is called "polyfills". Its purpose is to abstract away compiler \glref{API} differences for nonstandard features. Having compiler detecting "ifdef"s within the project code is hard to read, debug and test. Instead, the code is placed in a header file in "polyfills" directory, all compiler-dependent intrinsic are unified under a common interface and tested. In addition to development testing, these tests allow users compiling the code themselves to verify proper support on their current platform.

\midinsert
{\sbf Example: mulh64.} in "polyfills/mulh64.h" contains a set of functions calculating high bits of 64-bit multiplication. These functions are required to implement the RISC-V multiplication extension. However, the functionality is not directly available in the \glref{C} or \glref{C++} standard. The implementation uses 128-bit integers on GCC compatible compilers (if available), intrinsic for some sign combinations on \glref{MVSC} and manual fallbacks otherwise. This functionality is platform dependant, and as such, it must be covered by testing.
\endinsert
\midinsert
{\sbf Example: Byteswap.} "polyfills/byteswap.h" provides integer byte-swapping functions. For GCC (and compatible compilers, e.g., Clang) and \glref{MVSC}, compiler builtins are used. Otherwise, optimized fallbacks are provided. I have considered using the fallbacks only; however, swapping is necessary on hot code paths, and experiments at \url{https://godbolt.org} showed that some major compilers could not optimize it properly.
\endinsert
\midinsert
{\sbf Example: Endian detection.} Including the file "polyfills/endian_detection.h" ensures that either macro "__LITTLE_ENDIAN__" or "__BIG_ENDIAN__" is defined. If detection fails, the compilation immediately stops with a clear error message.
\endinsert

\midinsert
{\sbf Example: Endian utils.} "common/endian.h" library wraps the low-level \glref{C} functions (unified in "polyfills") into a better abstracted \glref{C++} and provides additional functionalities, e.g. conditional byte swap "byteswap_if(T value, bool cond)".
\endinsert

Polyfills library is considered low-level, and it does not introduce any new behavior. It only unifies the interfaces. More high-level libraries are placed elsewhere in {\tt common}.


\sec CMake Build System Generator

As part of project changes, I have suggested moving from QMake to CMake. QMake is now deprecated and is no longer developed. \glref{Qt} itself moved to CMake. CMake is the most widely used build system generator for \glref{C++}. It is well supported by development tools (IDEs, GitHub Actions, Nix), and it can generate various kinds of build files. Even though the CMake syntax contains many historical relicts, I believe that the current configuration is more straightforward to maintain than the QMake one.

In the following paragraphs, I provide some details on QMake deprecation,  present some problems that CMake helped me solve more elegantly, and in the end, I provide sources I found helpful when learning CMake.

{\sbf Note:} {\em I admit that my knowledge of QMake is slim, and I have no base to claim that the presented problems could not be solved in QMake. However, learning proper use of QMake seemed to be a comparable task to learning CMake.
Given the deprecation of QMake and better support of CMake, I have decided to favor CMake.}

\secc Qt Switch to CMake

In 2019, the \glref{Qt} company decided to switch the build of the \glref{Qt} framework itself to CMake. An article\cite[qtcmake] published in early 2021 presented the results. The main reason for the switch was the popularity of CMake among \glref{Qt} project developers. One example is the KDE community, which is behind many popular \glref{Qt} programs used on Linux. Another reason was that \glref{Qt} did not want to develop and maintain a build tool and instead focus on the development of the framework. The switch had a noticeable influence on CMake itself. CMake now supports tools needed by \glref{Qt} (e.g., the meta-object-compiler\fnote{Qt \glref{C++} preprocessor that enables reflection features and is needed to make most of the \glref{Qt} functionalities (like signal-slots) work.}) out of the box.  \glref{Qt} also influenced support of pre-compiled headers, Unity builds, and iOS support.

\secc Build Targets Relationships

Previously all relationships between build targets were specified manually by relative paths (see example below) in each built target that used them. Instead, CMake brings the concept of targets. A target is a collection of information necessary to build (called PRIVATE properties) and link (called PUBLIC properties.) an executable or a library. All properties are specified at the point of target creations. In other parts of the build configuration, targets are only referred by their names.

\midinsert
\begtt

  LIBS += -L$$OUT_PWD/../os_emulation/$${LIBS_SUBDIR}  -los_emulation
  LIBS += -L$$OUT_PWD/../machine/$${LIBS_SUBDIR} -lmachine -lelf
  LIBS += -L$$OUT_PWD/../assembler/$${LIBS_SUBDIR} -lassembler -lelf

  PRE_TARGETDEPS += $$OUT_PWD/../os_emulation/$${LIBS_SUBDIR}/\
                    libos_emulation.a
  PRE_TARGETDEPS += $$OUT_PWD/../machine/$${LIBS_SUBDIR}/libmachine.a
  PRE_TARGETDEPS += $$OUT_PWD/../assembler/$${LIBS_SUBDIR}/\
                    libassembler.a

\endtt
{\sbf Example.} {\em QMake code used to link dependencies of the "gui" executable.}

\begtt

  target_link_libraries(gui
        PRIVATE machine os_emulation assembler)

\endtt
{\sbf Example.} {\em CMake alternative to the previous example. Notice that "elf" is not mentioned in the new version. The LibElf library is not directly used by the "gui" target, and therefore it is not mentioned in its CMakeLists.txt. It is hidden in the "machine" target as a transitive public dependency. }
\endinsert


\secc Configure-time choice of dependencies

Typically, QtRVSim uses the system LibElf library\fnote{A library for handling ELF executables.}. However, in special cases, we want to fall back to a self-build, statically linked version. Originally, this was the case of the \glref{WASM} release, which cannot dynamically link libraries. In QtMips, this was not resolved as the \glref{WASM} target was new. There was a git branch where  LibElf was built and linked unconditionally.
Now in CMake, I have a target interface, which is used the same way in the whole project. At configure time, the interface either points to the system library or a local CMake target. Another advantage is that if CMake fails to find LibElf in the system, it automatically falls back to a static version. It is also helpful for Windows and macOS. As my Greek colleague informed me, there no ARM version of LibElf in Homebrew. I also expose a CMake configuration option to force static version usage.

\secc Config Defaults And Overrides

CMake provides a mechanism to specify the default properties of the whole project. I use this to set warning levels, \glref{C++} standard, and add runtime sanitizers to debug builds.

\secc Build and Run Tests

QtMips uses Bash scripts to build and run tests. It has to create a test build directory, invoke QMake and make, run the tests and determine results. CMake integrates a tool for test management called CTest. Test run and build tasks are now part of the same build file (make/Ninja).

\secc Learning CMake

My experience with CMake before this project had only been with trivial projects. Therefore, I had to understand it enough to make the switch possible. Here are some resources that I found helpful:

{\sbf Modern CMake online book}\urlnote{https://cliutils.gitlab.io/modern-cmake} provides a complete basic introduction to CMake as a build system generator as well as a programming language. It highlights modern features that make development much more effortless.

{\sbf CMake and \glref{Qt} article by KDAB}\urlnote{https://www.kdab.com/wp-content/uploads/stories/KDAB-whitepaper-CMake.pdf} provides more detail on CMake usage with \glref{Qt}.

{\sbf C++Now 2017: Daniel Pfeifer Effective CMake}\urlnote{https://www.youtube.com/watch?v=bsXLMQ6WgIk} conference talk presents and explains the concepts of modern, target-oriented CMake. I found the talk very useful to understand which parts of CMake I should use. CMake legacy \glref{API} can sometimes be confusing.

{\sbf Mastering CMake: Ken Martin, Bill Hoffman} is a reliable resource on CMake. I have not read it directly, but many helpful StackOverflow posts and blogs cite from it.

\sec GitHub CI Tests

Providing students with a malfunctional simulator can profoundly affect students' understanding of the subject, and their motivation to continue. Every developer makes mistakes, and therefore our only chance to reduce the risk is proper testing. Given that GitHub now provides unlimited \glref{CPU} time to run continuous integration tasks (\glref{CI}), it makes perfect sense to automate testing on every push. Moreover, \glref{CI} can run the tests on multiple platforms to eliminate accidental platform-dependent code. From the table with download statistics (\ref[github_downloads] in the appendix), it is evident that we cannot dismiss proper Windows support.

Therefore, I have implemented a \glref{CI} testing procedure using GitHub Actions\urlnote{https://docs.github.com/en/actions}. It builds QtRVSim for Ubuntu, macOS, and Windows and runs all tests on every git push. After examining the download statistics (\ref[launchpad_downloads]), Ubuntu 18 was added as a test platform in addition to Ubuntu 20. In addition, the WASM target is built. It does not contain tests, but given many specific problems of the WASM compilation, testing the ability to build it seems appropriate. In case of problems, the author is notified by email.

The \glref{CI} manifest can be later used for the continuous delivery of binaries for Windows and macOS. The CI procedure already exposes the resulting files for debugging purposes, e.g., examining macOS bundle structure or manually testing WASM release. This topic is also discussed in the next chapter.

\sec Logging Library

The introduction of the library was not intended to be part of my thesis. However, during my work, I have met and sometimes created many temporary "printf" statements. Some of them were commented out, some in "#if false" or "if (false)". Such a situation is not ideal for maintenance, so after consulting with my supervisor, I started searching for a logging library. First, I have collected requirements from my colleague, my supervisor, and myself:
\begitems
* Lightweight - we only need simple logging with minimal overhead.
* Readable log statements that can serve as comments.
* Runtime filtering by category.
* Crossplatform - e.g., there is no standard output by default on Windows for GUI applications.
\enditems

\url{https://cpp.libhunt.com/libs/logging} provided me with a reasonable listing of logging libraries. After analyzing the available libraries, I have found that the main differentiator is the printing mechanism. In \glref{C++} , there are generally 3 options: "printf", "std::iostreams", "std::fmt/fmtlib"\urlnote{https://github.com/fmtlib/fmt}. "std::fmt" is only available in C++20. The "fmtlib" library is the most performant with very readable "printf"-like syntax, and support for \glref{C++} custom object printing; however, it would bring another 16 thousand lines of code to the project. "std::iostreams" support \glref{C++} object printing; however, the syntax is highly unreadable, and statefulness makes them hard to use. "printf" does not support \glref{C++} objects and may be problematic on Windows.

After long consideration, I have chosen the logging library already present in \glref{Qt}. It supports "printf" syntax and provides a function to print \glref{Qt} types. It is already part of the codebase. And it has a powerful category filtering mechanism that does not use program arguments. It is configured to use a native sink on all platforms. It even works in the WASM release, where it uses the JavaScript console.

I have added simple wrapper macros "LOG", "INFO", "WARN", and "ERROR" which implicitly use a category defined in the file.

Other serious candidates were: Necrolog\urlnote{https://github.com/fvacek/necrolog} (very lightweight with regex filtering, developed by a coworker of my supervisor, "std::iostreams") and spdlog\urlnote{https://cpp.libhunt.com/spdlog-alternatives}("fmtlib", no category filtering).